function fixRut(rut, e) {
    var r = rut.value;
    var key = (e.which) ? e.which : event.keyCode;
    if ((key > 47 && key < 58) || (key > 95 && key < 106) || key === 75 || key === 8 || key === 46) {
        r = r.replace(/\./g, '');
        r = r.replace(/-/g, '');
        if(r.length > 1 && r.length < 10) {
            r = orderRut(r);
        } else if (r.length > 9) {
            r = r.slice(0, -1);
            r = orderRut(r);
        }
        rut.value = r;
    } else {
        if (r.length > lengthRut) {
            r = r.slice(0, -1);
            rut.value = r;
        }
    }

    lengthRut = r.length;
}

function orderRut(r) {
    if(r.length > 1 && r.length < 5) {
        var dv = r.slice(-1);
        r = r.slice(0, -1) + '-' + dv;
    } else if (r.length > 1 && r.length < 10){
        var dv = r.slice(-1);
        r = r.slice(0, -1);
        var fix = r;
                r = "-" + dv;
        while(fix.length > 0) {
            var sub = fix.slice(-3);
            fix = fix.slice(0, -3);
            r = sub + "." + r;
        }
        r = r.replace(".-", "-");
    }

    return r;
}

function verificarRut(rutCompleto) {
    if(rutCompleto.length > 0) {
        rutCompleto = rutCompleto.replace(/\./g, '');
        rutCompleto = rutCompleto.replace(/-/g, '');
        var digv 	= rutCompleto.slice(-1);
        var rut 	= rutCompleto.slice(0, -1);
        if ( digv == 'K' ) digv = 'k' ;
                
        var M=0,S=1;
        for(;rut;rut=Math.floor(rut/10))
            S=(S+rut%10*(9-M++%6))%11;
        var dv = S?S-1:'k';
        if(dv.toString() !== digv) alert('El rut ingresado no es válido');
    }
}